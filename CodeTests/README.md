#DEVELOPER TEST

**Target:** Backend Developer / Fullstack Developer

**Version:** 1.0


###INTRODUCTION
It's up to you if you want to create Console Projects, Unit Test projects or Web MVC projects.
All projects must be completed with C# and .NET 4.8 or .NET 5 or later

You decide if you solve all tasks within one project or separate projects for each task or a combination of each.
If you choose to build separate projects please name your projects after the task and number for example "task03".

It's not important that it actually works, it's more important the code you write to complete the task.

It's fully OK to just have you code return the HTML code whenever the task says create a HTML page and do something

You can use any NUGET packages.

If you're in doubt with anything, the tasks, technologies etc. reach out to Kevin
* Email: ks@co3.dk  
* Slack: ks@co3.dk
* LinkedIn: https://www.linkedin.com/in/kevinsteffer/

###Deliver you code back to us
You can e-mail your solution and all you projects in a zip file or send a link to a repository on github.com.
* GitHub: Please give access to the user "kevinsteffer"

#Tasks

###Task 01: Inheritance
**Difficulty rating:** 2/10

**Objective:** Demonstrate you understanding of Object Oriented Programming

**Description:**
Create a Product object that is based on an interface with at least these properties:
* Id
* Name
* Number
* Price
* Stock

 and these methods:
 * Save()
 * GetProductById() - returns a product object
 * GetProductByNumber() - return a product object

Create a Status property of type "enum" with at least 3 members


###Task 02: Instance control
**Difficulty rating:** 7/10

**Objective:** Demonstrate your programming knowledge to achieve this task

**Description:**
Create a "Store"-class from where you can retrieve a list of product objects from /Data/Products.json
and a list of group objects from /Data/Groups.json and last but not least one object that contains all products
grouped by their group relation to the Groups.json file.
It's important that there only can be one single store in the application


###Task 03: Integration: price updates
**Difficulty rating:** 4/10

**Objective:** Demonstrate you skills in IO handling, reading files into you application

**Description:**
In your store you should be able to receive a CSV file (/Data/Prices.csv) with product Ids or
product numbers and prices for each of them
**Example:**
```
| ProductNumber |  Price  |
|---------------|---------|
| 001           |  99.95  |
| 002           |  120.95 |
| 003           |  149.95 |
| 004           |  199.95 |
etc.
```
When you receive the CSV file the product prices should be updated in your application


###Task 04: Navigation
**Difficulty rating:** 5/10

**Objective:** Demonstrate that you can load data and work with HTML

**Description:**
Build one single webpage that displays a navigation based on the /Data/Navigation.json filev


###Task 05: Navigation Recursion
**Difficulty rating:** 6/10

**Objective:** Demonstrate that you can load data recursively in your application

**Description:**
Build one single webpage that displays the navigation based on the /Data/Navigation-2.json file  
where you are standing on the "About" page and where you render all the further navigation levels
as a left navigation where you make use of a recursive method that can render any amount of levels.


###Task 06: Receive data
**Difficulty rating:** 3/10

**Objective:** Demonstrate that you can receive data from a form and send it to an e-mail address (hint: Security)

**Description:**
There is a sample form you can use in /Data/form.html
You can do this in an MVC project or just a method that receives the information


###Task 07: Caching
**Difficulty rating:** 5/10

**Objective:** Demonstrate that you know how to use default .NET caching

**Description:**
Create a cache for the prices in "Task 03" and if you've got time make the cache update when the CSV file gets updated.


###Task 08: Dependency Injection
**Difficulty rating:** 8/10

**Objective:** Show you knowledge within Dependency Injection design pattern

**Description:**
Create a Store-class that returns some products with a GetProductById method and log with NLog when the method
can't find any product with the id parameter in the /Data/Products.json file.
It's important that the "Store"-class doesn't have a direct dependency to NLog so that you can Mock a Logger class
and have it use a different logging library without the need to recompile your project.


###Task 09: Databases
**Difficulty rating:** 5/10

**Objective:** Show you knowledge in RDBMS systems

**Description:**
Create a database (Microsoft SQL Server) where you can store the data from the files in the database
* /Data/Products.json
* /Data/Groups.json
* /Data/Prices.csv

And where you can keep track on when the data records was created, updated and by whom.
Optimize the tables as much as you can so that we can see how much you know about RDBMS capabilities.
Create some code that can import the data from the files into the tables.
Include a bit of logging and error management.
You should consider these data coming from an external IT system (hint: Security)


###Task 10: NoSQL (Extra - not mandatory)
**Difficulty rating:** 7/10

**Objective rating:** show us your experience with working with NoSQL databases (Lucene.NET, MongoDB, etc. your choice)

**Description:** 
Create a NoSQL database and create som code that imports the data from the /Data/Products.json 
file into the database.
Create some code that can get products from the database.  