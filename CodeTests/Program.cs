﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CodeTests.Printing_tools;

// Tasks:
using CodeTests.Task01_Inheritance;
using CodeTests.Task01_Inheritance.Delegates;
using CodeTests.Task02_Ecommerce_Store;

// Reoccuring task names:
using Product1 = CodeTests.Task01_Inheritance.Product;
using Product2 = CodeTests.Task02_Ecommerce_Store.Models.Product;
using Store2 = CodeTests.Task02_Ecommerce_Store.Store;
using Store3 = CodeTests.Task03_Integration_Price_Updates.Store;

namespace CodeTests
{
	public class Program
	{
		public static void Main(string[] args)
		{
			// Outcomment code to test different code tasks:
			// RunTask1();
			// RunTask2();
			RunTask3_7(); // Contains both task 3 and 7.

			Console.ReadLine();

		}

		/// <summary>
		/// Example of the methods and classes of task1 being put to use
		/// </summary>
		public static void RunTask1()
        {
			// Product object example:
			Product1 product = new Product1()
			{
				Id = 1,
				Name = "Vacuum cleaner",
				Number = 217326,
				Price = 2500,
				Stock = 150,
				Status = StatusEnum.inStock
			};

			string filename = @"Data\products.csv";

			// Saves object using SaveObject delegate
			product.Save(filename, ObjectData.SaveObject);

			// Gets created product by Id and Number
			Product1 productById = product.GetProductById(product.Id, filename);
			Product1 productByNumber = product.GetProductByNumber(product.Number, filename);

			// Checks if fetched product prints out correct names.
			Console.WriteLine(productById.Name);
			Console.WriteLine(productByNumber.Name);
		}

		/// <summary>
		/// Example of the methods and classes of task2 being put to use
		/// </summary>
		public static void RunTask2()
        {
			Store2 store = new Store2();

			// Catches error thrown from second store being created. 
			try
            {
				Store2 store2 = new Store2();
			} 
			catch (Exception ex)
            {
				Console.WriteLine(ex.Message);
			}

			// File to fecth json data from.
			string productFile = "Data/Products.json";
			string groupFile = "Data/Groups.json"; 

			// Printing out list of products by name:
			Console.WriteLine("Product list:");
			Console_printing.PrintAllElementNames(store.GetProducts(productFile));

			// Printing out list of groups by name
			Console.WriteLine("\nGroup list:");
			Console_printing.PrintAllElementNames(store.GetGroups(groupFile));

			// Printing out ordered list of products by name:
			// note: I have altered product 1 in the Product.json to be in group 2, to show that it works!
			Console.WriteLine("\nOrdered product list:");
			Console_printing.PrintAllElementNames(store.GetGroupOrderedProducts(productFile));

		}

		/// <summary>
		/// This both contains task 3 and 7.
		/// Example of the methods and classes of task3 being put to use
		/// 
		/// Note: To test auto update in console, go to Prices.csv in bin/debug/Data and change a value. (build project to acces files)
		/// </summary>
		/// 
		/// <important>
		/// I Didn't finish this so cache key is not updated -> only the first cache is created and the value can therefore currently only change 
		/// once when altering the file
		/// </important>
		public static void RunTask3_7()
        {
			// Creates store.
			Store3 store = new Store3();

			string filename = "Data/Prices.csv";

			// Fetches all data from csv file, and prints in store 
			store.DisplayProductElement(store.GetProductList(filename));
		}
	}
}