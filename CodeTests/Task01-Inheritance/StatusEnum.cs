﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTests.Task01_Inheritance
{
    public enum StatusEnum
    {
        [Description("In stock")]  inStock = 1 ,
        [Description("Outof stock")] outOfStock = 2,
        [Description("Remote storage")] remoteStorage = 3
    }
}
