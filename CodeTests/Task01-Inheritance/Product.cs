﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTests.Task01_Inheritance
{
    public class Product : IProduct<Product>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public double Price { get; set; }
        public int Stock { get; set; }
        public StatusEnum Status { get; set; }

        /// <summary>
        /// Saves the current object
        /// </summary>
        /// <param name="filename">The file to save to</param>
        /// <param name="SaveProduct">The delegate saving method</param>
        public void Save(string filename, Action<Product, string> SaveProduct)
        {
            SaveProduct(this, filename);
        }

        /// <summary>
        /// Fetches a product by Id
        /// </summary>
        /// <param name="Id">The Id of the considered product</param>
        /// <param name="filename">The file to search</param>
        /// <param name="ReadProducts">Delegate which fetchins a list of products from file</param>
        /// <returns></returns>
        public Product GetProductById(int id, string filename)
        {
            return GetObjects(filename).Where(r => r.Id == id).SingleOrDefault();
        }

        /// <summary>
        /// Fetches a product by Number
        /// </summary>
        /// <param name="Number">The Name of the considered product</param>
        /// <param name="filename">The file to search</param>
        /// <param name="ReadProducts">Delegate which fetchins a list of products from file</param>
        /// <returns></returns>
        public Product GetProductByNumber(int number, string filename)
        {
            // Using Linq to filter bu number
            return GetObjects(filename).Where(r => r.Number == number).SingleOrDefault();
        }

        /// <summary>
        /// Fetch list of products from file.
        /// </summary>
        /// <param name="filename"> filename to fetch from</param>
        /// <returns>Product list</returns>
        private static IEnumerable<Product> GetObjects(string filename)
        {
            IEnumerable<Product> records;

            using (StreamReader reader = new StreamReader(filename))
            {
                using (CsvReader csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    records = csv.GetRecords<Product>().ToList();
                }
            }

            return records;
        }
    }
}
