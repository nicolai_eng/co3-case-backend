﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTests.Task01_Inheritance
{
    public interface IProduct<T>
    {
        int Id { get; set; }
        string Name { get; set; }
        int Number { get; set; }
        double Price { get; set; }
        int Stock { get; set; }

        void Save(string filename, Action<T, string> product);
        T GetProductById(int Id, string filename);
        T GetProductByNumber(int Id, string filename);

    }
}
