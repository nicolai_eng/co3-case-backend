﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace CodeTests.Task01_Inheritance.Delegates
{
    // Module class for product related methods.
    public static class ObjectData
    {
        /// <summary>
        /// Saves an object in given file
        /// </summary>
        /// <typeparam name="T">The type of the input object</typeparam>
        /// <param name="obj">Object to save</param>
        /// <param name="filename">File to save to</param>
        public static void SaveObject<T>(T obj, string filename)
        {
            using (StreamWriter writer = new StreamWriter(filename))
            {
                using (CsvWriter csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.WriteHeader<T>();
                    csv.NextRecord();
                    csv.WriteRecord(obj);
                }
            }
        }
    }
}
