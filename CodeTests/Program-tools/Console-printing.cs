﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTests.Printing_tools
{
    public static class Console_printing
    {
		public static void PrintAllElementNames<T>(IEnumerable<T> inputList)
		{
			try
			{
				foreach (dynamic el in inputList)
				{
					Console.WriteLine(el.Name);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
