﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTests.Task03_Integration_Price_Updates
{
    public class Product
    {
        public string ProductNumber { get; set; }
        public string Price { get; set; }
    }
}
