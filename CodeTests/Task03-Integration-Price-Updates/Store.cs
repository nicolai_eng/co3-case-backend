﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CodeTests.Task03_Integration_Price_Updates
{
    public class Store
    {
        /// <summary>
        /// Constructor triggers FileSystemWatcher which triggers when file-changes occur.
        /// </summary>
         public Store()
        {
            var watcher = new FileSystemWatcher();
            watcher.Path = "Data";

            watcher.NotifyFilter = NotifyFilters.LastWrite;

            watcher.Filter = "*.*";
            watcher.Changed += OnChanged;
            watcher.EnableRaisingEvents = true;
        }

        /// <summary>
        /// Prints 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            // somettimes the onchanged triggers mulitple times due to known problem (https://stackoverflow.com/questions/1764809/filesystemwatcher-changed-event-is-raised-twice)
            // To avoid exceptions from mulitple simoultanious triggers, a try-catch is used.
            try
            {
                // Didn't finish this so key update doesn't work -> only the first cache is created
                ObjectCache cache = MemoryCache.Default;
                string CacheKey = "cacheAvailable";

                if (cache.Contains(CacheKey))
                {
                    DisplayProductElement((IEnumerable<Product>)cache.Get(CacheKey));
                }
                else
                {
                    IEnumerable<Product> productList = GetProductList("Data/Prices.csv");
                    DisplayProductElement(productList);

                    CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                    cache.Add(CacheKey, productList, cacheItemPolicy);
                }
            } catch
            {
            }
        }

        /// <summary>
        /// Extracts list of products from csv file.
        /// </summary>
        /// <returns>List of products</returns>
        public IEnumerable<Product> GetProductList(string filename)
        {

            using (StreamReader reader = new StreamReader(filename))
            {
                // Contains object names:
                reader.ReadLine();

                // Array containing each csv line:
                string[] data = reader.ReadToEnd().Split('\n');

                List<Product> products = new List<Product>();

                // Creates list of product objects.
                foreach(string line in data)
                {
                    products.Add(GetProduct(
                        Regex.Replace(line, "[^0-9_.]", " ") // removes all special signs.
                    ));
                }

                return products;
            }
        }

        /// <summary>
        /// Extracts the inputs from a line and casts them into a product object.
        /// </summary>
        /// <param name="line">String line of data from csv file</param>
        /// <returns>Product</returns>
        private Product GetProduct(string line)
        {
            List<string> objectList = new List<string>();

            // Only selects non-empty elements.
            foreach (string el in line.Split(' '))
            {
                if (el != "")
                {
                    objectList.Add(el);
                }
            }

            // Returns products object.
            return new Product()
            {
                ProductNumber = objectList[0],
                Price = objectList[1]
            };
        }

        /// <summary>
        /// Prints store console
        /// </summary>
        /// <param name="products">List of product objects</param>
        public void DisplayProductElement(IEnumerable<Product> products)
        {
            Console.Clear();

            StringBuilder printLine = new StringBuilder();
            printLine.AppendLine("| ProductNumber | Prices |");
            printLine.AppendLine("|---------------|--------|");

            foreach (Product product in products)
            {
                int numberLength = product.ProductNumber.Length;
                int priceLength = product.Price.Length;

                printLine.Append("| " + product.ProductNumber);

                for (int i = 0; i < 13 - numberLength; i++)
                {
                    printLine.Append(" ");
                }

                printLine.Append(" | " + product.Price);


                for (int i = 0; i < 6 - priceLength; i++)
                {
                    printLine.Append(" ");
                }

                printLine.AppendLine(" |");
            }

            Console.WriteLine(printLine);
        }
    }
}
