﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTests.Task02_Ecommerce_Store.Models
{
    public class ProductsJson
    {
        public List<Product> Products { get; set; }
    }
}
