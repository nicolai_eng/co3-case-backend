﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTests.Task02_Ecommerce_Store.Models
{
    internal class GroupsJson
    {
        public List<Group> Groups { get; set; }
    }
}
