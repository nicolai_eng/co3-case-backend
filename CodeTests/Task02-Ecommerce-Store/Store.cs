﻿using CodeTests.Task02_Ecommerce_Store.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTests.Task02_Ecommerce_Store
{
    public class Store
    {
        private static bool storeExists { get; set; } = false;

        /// <summary>
        /// Constructor throws error if a store already exist
        /// </summary>
        public Store()
        {
            if (storeExists == true)
            {
                throw (new Exception("Only one store can exist"));
            }

            storeExists = false;
        }

        /// <summary>
        /// Extracts list of product objects from Json file.
        /// </summary>
        /// <returns>List of product objects</returns>
        public IEnumerable<Product> GetProducts(string filename)
        {
            ProductsJson products;

            // Reads and deserializes json:
            using (StreamReader reader = new StreamReader(filename))
            {
                string jsonString = reader.ReadToEnd();
                products = JsonConvert.DeserializeObject<ProductsJson>(jsonString);
            }
            return products.Products;
        }

        public IEnumerable<Product> GetGroupOrderedProducts(string filename)
        {
            IEnumerable<Product> products = GetProducts(filename);

            return products.OrderBy(p => p.Group);


        }

        public IEnumerable<Group> GetGroups(string filename)
        {
            GroupsJson groups;

            // Reads and deserializes json:
            using (StreamReader reader = new StreamReader(filename))
            {
                string jsonString = reader.ReadToEnd();
                groups = JsonConvert.DeserializeObject<GroupsJson>(jsonString);
            }
            return groups.Groups;
        }
    }
}
